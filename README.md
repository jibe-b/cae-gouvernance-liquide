# CAE à gouvernance liquide

## Principe 1 : Prise de décision par vote liquide

### Granularité et catégorisation

La prise de décision liquide est basée sur une granularité fine de prise de décision (une question -> une prise de décision, et non pas un lot de décisions très diverses -> une loi).

Les décisions peuvent être regroupées par catégories par tout individu. La CAE peut proposer une catégorisation, qui sera à égalité avec les catégorisations réalisées par un·e membre.

### Vote proxy

Chaque individu a le droit de vote sur chacune des décisions.

Par ailleurs, chaque individu peut déléguer le vote pour une question ou une catégorie de questions à un autre individu (proxy).

Il est possible de déléguer différentes catégories de questions à différentes personnes.

Cette délégation est révoquable à tout moment.

## Principe 2 : Quorum de participation

Afin de veiller à une dynamique importante de réflexion, différents taux sont fixés pour la validité d'un vote :

- nombre de voteurs proxy (n'ayant pas délégué leur vote) exprimés 
- nombre de votants (proxy et ayant délégué leur vote) exprimés

- nombre de voteurs proxy (n'ayant pas délégué leur vote) pour
- nombre de votants (proxy et ayant délégué leur vote) pour

## Thématiques relevant du processus de décision liquide

Les thématiques soumises à prise de décision liquide sont déterminées collectivement et couvrent :

- la politique économique de la CAE
- les actions et dynamiques internes
- la communication et la gestion de marque

## Mise en pratique

Un rôle d'organisateur·rice de vote (également gestionnaire de catégories) doit être assuré au sein de la CAE.

Un logiciel comme Loomio ou Sovereign peut être utilisé.
